# Hex-to-RGB Conversion API

## Overview
This project is a Node.js application using the Express framework to provide a REST API for converting hexadecimal color codes to their corresponding RGB values. It is designed to handle requests efficiently and includes error handling for invalid inputs.

## Features
- **Hex to RGB Conversion**: Convert hexadecimal color values to RGB format.
- **Error Handling**: Robust error responses for invalid input.
- **Unit and Integration Testing**: Comprehensive tests using Mocha, Chai, and Supertest.

## Getting Started

### Prerequisites
- Node.js
- npm (Node Package Manager)

### Installation
1. **Clone the repository:**
   ```bash
   git clone https://gitlab.com/superilyushagrief/integrationtesting
   ```
2. **Navigate to the project directory:**
   ```bash
   cd Intergration
   ```
3. **Install dependencies:**
   ```bash
   npm install
   ```

### Running the Server
- Run the server using:
  ```bash
  npm start
  ```
- By default, the server will run on [http://localhost:3000](http://localhost:3000).

## Usage
Send a POST request to `/api/hex-to-rgb` with a JSON payload containing the hex value.
- **Endpoint:** POST /api/hex-to-rgb
- **Payload:** `{ "hex": "#FFFFFF" }`

### Example Request
```bash
curl -X POST http://localhost:3000/api/hex-to-rgb \
-H "Content-Type: application/json" \
-d '{"hex": "#FFFFFF"}'
```

### Response
A successful request returns a JSON object with the RGB values:
```json
{
  "r": 255,
  "g": 255,
  "b": 255
}
```

## Running Tests
Execute unit and integration tests by running:
```bash
npm test
```

## Contributing
Contributions to the project are welcome. Please follow these steps:
1. Fork the repository.
2. Create a new branch (`git checkout -b feature-branch`).
3. Make your changes and commit (`git commit -am 'Add a feature'`).
4. Push to the branch (`git push origin feature-branch`).
5. Open a pull request.

## License
This project is licensed under the [MIT License](LICENSE).