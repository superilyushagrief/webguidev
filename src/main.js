const express = require('express');
const hexToRgb = require("../src/converter");
const app = express();
app.use(express.json());

app.post('/api/hex-to-rgb', (req, res) => {
    const hex = req.body.hex;
    if (!hex) {
        return res.status(400).json({ error: 'Hex value is required' });
    }

    try {
        const rgb = hexToRgb(hex);
        res.json(rgb); // Send response as JSON!!! (resolved)
    } catch (error) {
        res.status(400).json({ error: error.message }); // Send error as JSON
    }
});

if (require.main === module) {
    const port = process.env.PORT || 3000;
    app.listen(port, () => console.log(`Listening on port ${port}...`));
}

module.exports = app;

