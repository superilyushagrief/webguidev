function hexToRgb(hex) {
    if (!hex) {
        throw new Error('Invalid Hex format');
    }
    if (hex.startsWith('#')) {
        hex = hex.substring(1);
    }
    if (hex.length === 3) {
        hex = hex[0] + hex[0]
            + hex[1] + hex[1]
            + hex[2] + hex[2];
    }
    const validHex = /^[a-fA-F0-9]{6}$/;
    if (!validHex.test(hex)) {
        throw new Error('Invalid Hex format');
    }
    const r = parseInt(hex.substring(0, 2), 16);
    const g = parseInt(hex.substring(2, 4), 16);
    const b = parseInt(hex.substring(4, 6), 16);
    return { r, g, b };
}

module.exports = hexToRgb;